from quotebot.adspaces import AdSpaces
from quotebot.auction import BlogAuctionTask


class AutomaticQuoteBot(object):
    def send_all_quotes(self, mode):
        blogs = AdSpaces.get_ad_spaces()
        print(blogs)
        for blog in blogs:
            blog_auction_task = BlogAuctionTask()
            blog_auction_task.price_and_publish(blog, mode)