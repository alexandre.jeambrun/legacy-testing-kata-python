from quotebot.techblogs import TechBlogs


class AdSpaces(object):
    __cache = dict()

    @staticmethod
    def get_ad_spaces():
        if "blogs list" in AdSpaces.__cache:
            return AdSpaces.__cache["blogs list"]

        # FIXME should return only blogs that start with "T"
        AdSpaces.__cache["blogs list"] = TechBlogs.list_all_blogs()

        return AdSpaces.__cache["blogs list"]