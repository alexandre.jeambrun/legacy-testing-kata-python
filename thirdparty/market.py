import math
import os
import random

import pymsgbox


class MarketStudyVendor(object):
    def averagePrice(self, blog):
        if os.environ.get('license') is None:
            pymsgbox.alert("Missing license !", "Stupid license", "Ok")
            raise Exception("No License!")

        return int(math.fabs(hash(blog)) * random.random() / 1e15)
