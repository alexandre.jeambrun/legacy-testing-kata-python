import pymsgbox


class QuotePublisher(object):
    __instance = None

    def __new__(cls):
        if cls.__instance is None:
            cls.__instance = object.__new__(cls)
        return cls.__instance

    @staticmethod
    def instance():
        return QuotePublisher()

    def publish(self, proposal):
        pymsgbox.alert("You've pushed a dummy auction to a real ads platform, the business is upset!", "Big fail!", "You are Fired")